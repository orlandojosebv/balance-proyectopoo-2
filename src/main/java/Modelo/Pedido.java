/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author dcuad
 */
public class Pedido {

    private int idPedido;
    private int idCliente;
    private String estadoDelPedido;
    private String fechaPedido;
    private int cantidadPares;
    private int idProducto;
    private int idEmpleado;

    public Pedido() {
    }

    public Pedido(int idPedido, int idCliente, String estadoDelPedido, String fechaPedido, int cantidadPares, int idProducto, int idEmpleado) {
        this.idPedido = idPedido;
        this.idCliente = idCliente;
        this.estadoDelPedido = estadoDelPedido;
        this.fechaPedido = fechaPedido;
        this.cantidadPares = cantidadPares;
        this.idProducto = idProducto;
        this.idEmpleado = idEmpleado;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getEstadoDelPedido() {
        return estadoDelPedido;
    }

    public void setEstadoDelPedido(String estadoDelPedido) {
        this.estadoDelPedido = estadoDelPedido;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public int getCantidadPares() {
        return cantidadPares;
    }

    public void setCantidadPares(int cantidadPares) {
        this.cantidadPares = cantidadPares;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

}
