/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author dcuad
 */
public class Tarea {
    private int id_empleado;
    private int id_pedido;
    private int id;

    public Tarea() {
    }

    public Tarea(int id_empleado, int id_pedido, int id) {
        this.id_empleado = id_empleado;
        this.id_pedido = id_pedido;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }
    
}
