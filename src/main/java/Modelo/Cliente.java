/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author dcuad
 */
public class Cliente extends Persona{

    private int id;
    

    public Cliente() {
    }

    public Cliente(int id, String nombre, String apellido, String correo, String telefono) {
        super(id, nombre, apellido, correo, telefono);
        this.id = id;
    }

    public Cliente(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Cliente = "+id+","+this.getNombre()+","+this.getApellido()+","+this.getCorreo()+","+this.getTelefono();
    }
    
    

}
