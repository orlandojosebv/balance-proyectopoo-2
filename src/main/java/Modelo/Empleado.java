/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author dcuad
 */
public class Empleado extends Persona{

    private int id;
    private String fecha_terminacion;
    private int cantidad_pares_realizados;
    private int pago_empleado;

    public Empleado() {
    }

    public Empleado(int id, String fecha_terminacion, int cantidad_pares_realizados, int pago_empleado, String nombre, String apellido, String correo, String telefono) {
        super(id, nombre, apellido, correo, telefono);
        this.id = id;
        this.fecha_terminacion = fecha_terminacion;
        this.cantidad_pares_realizados = cantidad_pares_realizados;
        this.pago_empleado = pago_empleado;
    }

    

    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha_terminacion() {
        return fecha_terminacion;
    }

    public void setFecha_terminacion(String fecha_terminacion) {
        this.fecha_terminacion = fecha_terminacion;
    }

    public int getCantidad_pares_realizados() {
        return cantidad_pares_realizados;
    }

    public void setCantidad_pares_realizados(int cantidad_pares_realizados) {
        this.cantidad_pares_realizados = cantidad_pares_realizados;
    }

    public int getPago_empleado() {
        return pago_empleado;
    }

    public void setPago_empleado(int pago_empleado) {
        this.pago_empleado = pago_empleado;
    }

    @Override
    public String toString() {
        return "Empleado = "+id+","+fecha_terminacion+","+cantidad_pares_realizados+","+pago_empleado+","+this.getNombre()+","+this.getApellido()+","+this.getCorreo()+","+ this.getTelefono();
    }

    
}
