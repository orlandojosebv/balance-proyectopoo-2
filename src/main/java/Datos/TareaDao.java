/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Tarea;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class TareaDao implements TareaServices {

    public static final String SQL_INSERT = "INSERT INTO tarea(id, id_empleado, id_pedido) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM tarea";
    public static final String SQL_CONSULTAID = "SELECT * FROM tarea WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM tarea WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE tarea  WHERE id = ?";

    @Override
    public int create(Tarea tarea) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, tarea.getId());
            ps.setInt(2, tarea.getId_empleado());
            ps.setInt(3, tarea.getId_pedido());
            registros = ps.executeUpdate();
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Tarea> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Tarea> tareas = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                int idEmpleado = res.getInt("id_empleado");
                int idPedido = res.getInt("id_pedido");
                Tarea tarea = new Tarea(id, idEmpleado, idPedido);
                tareas.add(tarea);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return tareas;
    }

    @Override
    public Tarea selectId(Tarea tarea) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Tarea registroTarea = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, tarea.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            int idEmpleado = res.getInt("id_empleado");
            int idPedido = res.getInt("id_pedido");
            registroTarea = new Tarea(id, idEmpleado, idPedido);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroTarea;
    }

    @Override
    public int update(Tarea tarea) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, tarea.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Tarea tarea) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, tarea.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
