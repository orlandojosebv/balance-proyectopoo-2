/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Empleado;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class EmpleadoDao implements EmpleadoServices {

    public static final String SQL_CONSULTA = "SELECT * FROM empleado";
    public static final String SQL_CONSULTAP = "SELECT * FROM persona";
    public static final String SQL_INSERT = "INSERT INTO empleado(id_empleado, fecha_terminacion, cantidad_pares_realizados, Pago_empleado) VALUES (?,?,?,?)";
    public static final String SQL_INSERTP = "INSERT INTO persona(cedula, nombre, apellido, correo, telefono) VALUES (?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM empleado WHERE id_empleado = ?";
    public static final String SQL_UPDATE = "UPDATE empleado SET fecha_terminacion = ?, cantidad_pares_realizados = ? , Pago_empleado = ? WHERE id = ?";
    public static final String SQL_UPDATEP = "UPDATE persona SET nombre = ?, apellido = ? , correo = ? , telefono = ? WHERE cedula = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM empleado WHERE id_empleado = ?";
    public static final String SQL_CONSULTAIDP = "SELECT * FROM persona WHERE cedula = ?";

    @Override
    public int create(Empleado empleado) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERTP);
            ps.setInt(1, empleado.getCedula());
            ps.setString(2, empleado.getNombre());
            ps.setString(3, empleado.getApellido());
            ps.setString(4, empleado.getCorreo());
            ps.setString(5, empleado.getTelefono());
            registros = ps.executeUpdate();
            
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, empleado.getCedula());
            ps.setString(2, empleado.getFecha_terminacion());
            ps.setInt(3, empleado.getCantidad_pares_realizados());
            ps.setInt(4, empleado.getPago_empleado());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Empleado> all() {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement pc = null;
        ResultSet res = null;
        ResultSet res2 = null;
        List<Empleado> empleados = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAP);
            pc = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            res2 = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                Empleado empleado = new Empleado();
                empleado.setId(id);
                empleado=this.selectId(empleado);
                empleados.add(empleado);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return empleados;
    }

    @Override
    public Empleado selectId(Empleado empleado) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement pc = null;
        ResultSet res = null;
        Empleado registroEmpleado = null;
        try {
            con = BaseDeDatos.getConnection();
            pc = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps = con.prepareStatement(SQL_CONSULTAIDP, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pc.setInt(1, empleado.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha_terminacion = res.getString("fecha_terminacion");
            int cantidad_pares_realizados = res.getInt("cantidad_pares_realizados");
            int pago_empleado = res.getInt("Pago_empleado");
            ps.setInt(1,empleado.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String correo = res.getString("correo");
            String telefono = res.getString("telefono");
            registroEmpleado = new Empleado(id, fecha_terminacion, cantidad_pares_realizados, pago_empleado, nombre, apellido, correo, telefono);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroEmpleado;
    }

    @Override
    public int update(Empleado empleado) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATEP);
            ps.setInt(5, empleado.getCedula());
            ps.setString(1, empleado.getNombre());
            ps.setString(2, empleado.getApellido());
            ps.setString(3, empleado.getCorreo());
            ps.setString(4, empleado.getTelefono());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, empleado.getPago_empleado());
            ps.setInt(2, empleado.getCantidad_pares_realizados());
            ps.setString(3, empleado.getFecha_terminacion());
            ps.setInt(4, empleado.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Empleado empleado) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, empleado.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }
}
