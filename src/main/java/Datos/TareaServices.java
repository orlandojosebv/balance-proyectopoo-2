/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Tarea;
import java.util.List;

/**
 *
 * @author dcuad
 */
public interface TareaServices {
    public int create(Tarea tarea);
    public List<Tarea> all();
    public Tarea selectId(Tarea tarea);
    public int update(Tarea tarea);
    public int delete(Tarea tarea);
}
