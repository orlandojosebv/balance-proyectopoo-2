/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Cliente;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class ClienteDao implements ClienteServices {
    public static final String SQL_INSERT = "INSERT INTO cliente(id_cliente) VALUES (?)";
    public static final String SQL_INSERTP = "INSERT INTO persona(id_persona, nombre, apellido, correo, telefono) VALUES (?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM cliente";
    public static final String SQL_CONSULTAP = "SELECT * FROM persona";
    public static final String SQL_CONSULTAIDP = "SELECT * FROM persona WHERE id_persona = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM cliente WHERE id_cliente = ?";
    public static final String SQL_DELETE = "DELETE FROM cliente WHERE id_cliente = ?";
    public static final String SQL_UPDATE = "UPDATE cliente WHERE id_cliente = ?";
    public static final String SQL_UPDATEP = "UPDATE persona SET nombre = ?, apellido = ?, correo = ? , telefono = ? WHERE id_persona = ?";


    @Override
    public int create(Cliente cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERTP);
            ps.setInt(1, cliente.getCedula());
            ps.setString(2, cliente.getNombre());
            ps.setString(2, cliente.getApellido());
            ps.setString(3, cliente.getCorreo());
            ps.setString(4, cliente.getTelefono());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, cliente.getCedula());
            registros = ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;

    }

    @Override
    public List<Cliente> all() {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement pc = null;
        ResultSet res = null;
        ResultSet res2 = null;
        List<Cliente> clientes = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAP);
            pc = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            res2 = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                Cliente cliente = new Cliente();
                cliente.setId(id);
                cliente = this.selectId(cliente);//SI SALE ERROR MIRA ACÁ PORQUE NO ESTOY SEGURO OJO!!!!!
                clientes.add(cliente);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return clientes;
    }

    @Override
    public Cliente selectId(Cliente cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement pc = null;
        ResultSet res = null;
        Cliente registroCliente = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAIDP, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pc = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            pc.setInt(1, cliente.getId());
            res = pc.executeQuery();
            res.absolute(1);
            
            int id_cliente = res.getInt("id");
            ps.setInt(1, cliente.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String correo = res.getString("correo");
            String telefono = res.getString("telefono");
            registroCliente = new Cliente(id_cliente, nombre, apellido, correo, telefono);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroCliente;

    }

    @Override
    public int update(Cliente cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATEP);
            ps.setString(1, cliente.getTelefono());
            ps.setString(2, cliente.getCorreo());
            ps.setString(3, cliente.getApellido());
            ps.setString(4, cliente.getNombre());
            ps.setInt(5, cliente.getCedula());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, cliente.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) { 
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Cliente cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, cliente.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
