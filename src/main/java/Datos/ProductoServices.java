/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Producto;
import java.util.List;

/**
 *
 * @author dcuad
 */
public interface ProductoServices {
    public int create(Producto producto);
    public List<Producto> all();
    public Producto selectId(Producto producto);
    public int update(Producto producto);
    public int delete(Producto producto);
}
