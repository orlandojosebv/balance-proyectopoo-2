/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Producto;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class ProductoDao implements ProductoServices {

    public static final String SQL_INSERT = "INSERT INTO producto(id_producto, suela, color) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM producto";
    public static final String SQL_CONSULTAID = "SELECT * FROM producto WHERE id_producto = ?";
    public static final String SQL_DELETE = "DELETE FROM producto WHERE id_producto = ?";
    public static final String SQL_UPDATE = "UPDATE producto SET suela = ?, color = ?  WHERE id_producto = ?";

    @Override
    public int create(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, producto.getId_producto());
            ps.setString(2, producto.getSuela());
            ps.setString(3, producto.getColor());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Producto> all() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Producto> productos = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id_producto");
                String suela = res.getString("suela");
                String color = res.getString("color");
                Producto producto = new Producto(id, suela, color);
                productos.add(producto);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return productos;
    }

    @Override
    public Producto selectId(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Producto registroProducto = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, producto.getId_producto());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id_producto");
            String suela = res.getString("suela");
            String color = res.getString("color");
            registroProducto = new Producto(id, suela, color);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroProducto;
    }

    @Override
    public int update(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, producto.getColor());
            ps.setString(2, producto.getSuela());
            ps.setInt(3, producto.getId_producto());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, producto.getId_producto());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }
}
