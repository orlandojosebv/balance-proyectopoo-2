/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Pedido;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class PedidoDao implements PedidoServices {

    public static final String SQL_INSERT = "INSERT INTO pedido(id_Pedido, id_cliente, estado_del_pedido, fecha_pedido, cantidad_pares, id_producto, id_empleado) VALUES (?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM pedido";
    public static final String SQL_CONSULTAID = "SELECT * FROM pedido WHERE id_Pedido = ?";
    public static final String SQL_DELETE = "DELETE FROM pedido WHERE id_Pedido = ?";
    public static final String SQL_UPDATE = "UPDATE pedido SET id_pedido = ? , id_cliente = ?, estado_del_pedido=?, fecha_pedido=?, cantidad_pares=?, id_producto=?, id_empleado=? WHERE id_Pedido = ?";

    @Override
    public int create(Pedido pedido) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, pedido.getIdPedido());
            ps.setInt(2, pedido.getIdCliente());
            ps.setString(3, pedido.getEstadoDelPedido());
            ps.setString(4, pedido.getFechaPedido());
            ps.setInt(5, pedido.getCantidadPares());
            ps.setInt(6, pedido.getIdProducto());
            ps.setInt(7, pedido.getIdEmpleado());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Pedido> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Pedido> pedidos = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int idPedido = res.getInt("id_pedido");
                int idCliente = res.getInt("id_cliente");
                String estadoDelPedido = res.getString("estado_del_pedido");
                String fechaPedido = res.getString("fecha_pedido");
                int cantidadPares = res.getInt("cantidad_pares");
                int idProducto = res.getInt("id_producto");
                int idEmpleado = res.getInt("id_empleado");
                Pedido pedido = new Pedido(idEmpleado, idPedido, estadoDelPedido, fechaPedido, cantidadPares, idProducto, idEmpleado);
                pedidos.add(pedido);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return pedidos;
    }

    @Override
    public Pedido selectId(Pedido pedido) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Pedido registroPedidos = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, pedido.getIdPedido());
            res = ps.executeQuery();
            res.absolute(1);
            int idPedido = res.getInt("id_pedido");
            int idCliente = res.getInt("id_cliente");
            String estadoDelPedido = res.getString("estado_del_pedido");
            String fechaPedido = res.getString("fecha_pedido");
            int cantidadPares = res.getInt("cantidad_pares");
            int idProducto = res.getInt("id_producto");
            int idEmpleado = res.getInt("id_empleado");
            registroPedidos = new Pedido(idEmpleado, idPedido, estadoDelPedido, fechaPedido, cantidadPares, idProducto, idEmpleado);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroPedidos;
    }

    @Override
    public int update(Pedido pedido) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, pedido.getIdEmpleado());
            ps.setInt(2, pedido.getIdProducto());
            ps.setInt(3, pedido.getCantidadPares());
            ps.setString(4, pedido.getFechaPedido());
            ps.setString(5, pedido.getEstadoDelPedido());
            ps.setInt(6, pedido.getIdCliente());
            ps.setInt(7, pedido.getIdPedido());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Pedido pedido) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, pedido.getIdPedido());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
