/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Cliente;
import java.util.List;

/**
 *
 * @author dcuad
 */
public interface ClienteServices {
    public int create(Cliente cliente);
    public List<Cliente> all();
    public Cliente selectId(Cliente cliente);
    public int update(Cliente cliente);
    public int delete(Cliente cliente);
}
