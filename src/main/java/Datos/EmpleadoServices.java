/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Empleado;
import java.util.List;

/**
 *
 * @author dcuad
 */
public interface EmpleadoServices {
    public int create(Empleado empleado);
    public List<Empleado> all();
    public Empleado selectId(Empleado empleado);
    public int update(Empleado empleado);
    public int delete(Empleado empleado);
}
