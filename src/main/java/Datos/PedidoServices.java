/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Pedido;
import java.util.List;

/**
 *
 * @author dcuad
 */
public interface PedidoServices {

    public int create(Pedido pedido);
    public List<Pedido> all();
    public Pedido selectId(Pedido pedido);
    public int update(Pedido pedido);
    public int delete(Pedido pedido);
}
