/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Datos.ProductoDao;
import Modelo.Producto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class TestProducto {

    public static void main(String[] args) {

        ProductoDao w = new ProductoDao();
        //w.create(new Producto(4, "negro caja", "celeste"));
        List<Producto> productos = new ArrayList<>();
        productos = w.all();
        for (int i = 0; i < productos.size(); i++) {
            System.out.println("nombre = " + productos.get(i).getId_producto());
        }
    }
}
