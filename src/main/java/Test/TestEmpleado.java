/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Datos.EmpleadoDao;
import Modelo.Empleado;
import Modelo.Tarea;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dcuad
 */
public class TestEmpleado {
    public static void main(String[] args) {
        
    
        EmpleadoDao r = new EmpleadoDao();
        r.create(new Empleado(1, "25/25/25",12,12000,"juanito","alimaña","prueba@gmail.com","32156942"));
        List<Empleado> empleados = new ArrayList<>();
        empleados = r.all();
        for (int i = 0; i < empleados.size(); i++) {
            System.out.println(empleados.get(i).toString());
        }
    }
}
